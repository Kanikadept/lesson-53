import React, {useState} from "react";
import "./ToDoForm.css"

const ToDoForm = ({addToDo}) => {
    const [content, setContent] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        addToDo(content);
        setContent('');
    }
    return (
        <form className="to-do-form" onSubmit={handleSubmit}>
            <input className="to-do-form_input" type="text" value={content} required onChange={(e) => setContent(e.target.value)}/>
            <input className="to-do-form_btn" type="submit" value="add to do"/>
        </form>
    );
}

export default ToDoForm;