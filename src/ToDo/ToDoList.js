import React, {useState} from "react";
import "./ToDoList.css";
import {nanoid} from "nanoid";
import ToDoForm from "../ToDoForm/ToDoForm";

const ToDoList = props => {
    const [toDo, setToDo] = useState([
        {id: nanoid(), content: "some text"},
        {id: nanoid(), content: "Hello world"},
        {id: nanoid(), content: "here some coffee"},
    ]);

    const addToDo = (content) => {
        setToDo([...toDo, {content, id: nanoid()}]);
    }

    const deleteToDo = (id) => {
        const index = toDo.findIndex(td => td.id === id);
        const toDoCopy = [...toDo];
        toDoCopy.splice(index, 1);
        console.log(index);
        setToDo(toDoCopy);
    }

    return (
        <>
            <ToDoForm addToDo={addToDo}/>
            <ul className="to-do-list">
                {
                    toDo.map((toDo, index) => {
                        return  <li className="to-do" key={toDo.id}>
                            <p className="to-do_text">{toDo.content}</p>
                            <button className="to-do_btn" onClick={() => deleteToDo(toDo.id)}>delete</button>
                        </li>
                    })
                }
            </ul>
        </>
    );
}

export default ToDoList;