import React from "react";
import './App.css';
import ToDoList from "./ToDo/ToDoList";

const App = () => {

  return (
    <div className="App">
        <div className="container">
            <ToDoList/>
        </div>
    </div>
  );
}

export default App;
